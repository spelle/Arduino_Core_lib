Arduino_Core_lib
================


Imported from https://github.com/spelle/Arduino_Core_lib


I. Clone the repo
-----------------
    git clone https://git.framasoft.org/spelle/Arduino_Core_lib.git

or

    git clone git@git.framasoft.org:spelle/Arduino_Core_lib.git

II. Initialize and update submodules
------------------
    git submodule init
    git submodule update

III. That's it
-----------------

Open Eclipse & import project

Enjoy !